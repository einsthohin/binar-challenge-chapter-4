function getPilihanComputer() {
    const comp = Math.random();
    if(comp < 0.33) return '.batu-img-comp';
    if(comp >= 0.33 && comp < 0.66) return '.kertas-img-comp';
    return '.gunting-img-comp';
}

function getHasil(comp, player) {
    if(player == comp) return 'DRAW!';
    if(player == 'batu') return (comp == 'kertas') ? 'COMPUTER WIN!' : 'PLAYER WIN!';
    if(player == 'kertas') return (comp == 'gunting') ? 'COMPUTER WIN!' : 'PLAYER WIN!';
    if(player == 'gunting') return (comp == 'batu') ? 'COMPUTER WIN!' : 'PLAYER WIN!';
}

const playerBatu = document.querySelector('.batu');
playerBatu.addEventListener('click', function() {
    playerBatu.style.backgroundColor = "#C4C4C4";
    playerBatu.style.borderRadius = "15px";
    playerBatu.style.display = "block";
    playerBatu.style.marginLeft = "auto";
    playerBatu.style.marginRight = "auto";

    const pilihanComputer = getPilihanComputer ();
    const pilihanPlayer = playerBatu.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const vs = document.querySelector('.hasil');
    vs.innerHTML = hasil;

    if( pilihanComputer == '.batu-img-comp' ) {
	const borderBatuComp = document.getElementById('border-batu-comp');
	borderBatuComp.setAttribute('class', 'border-batu-comp');

	const borderKertasComp = document.getElementById('border-kertas-comp');
	borderKertasComp.removeAttribute('class');

	const borderGuntingComp = document.getElementById('border-gunting-comp');
	borderGuntingComp.removeAttribute('class');

	} else if( pilihanComputer == '.gunting-img-comp' ) {
		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.setAttribute('class', 'border-gunting-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.removeAttribute('class');

	} else if( pilihanComputer == '.kertas-img-comp' ) {
		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.setAttribute('class', 'border-kertas-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.removeAttribute('class');
    };
});

const playerKertas = document.querySelector('.kertas');
playerKertas.addEventListener('click', function() {
    playerKertas.style.backgroundColor = "#C4C4C4";
    playerKertas.style.borderRadius = "15px";
    playerKertas.style.padding = "8px";

    const pilihanComputer = getPilihanComputer ();
    const pilihanPlayer = playerKertas.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const vs = document.querySelector('.hasil');
    vs.innerHTML = hasil;

    if( pilihanComputer == '.batu-img-comp' ) {
	const borderBatuComp = document.getElementById('border-batu-comp');
	borderBatuComp.setAttribute('class', 'border-batu-comp');

	const borderKertasComp = document.getElementById('border-kertas-comp');
	borderKertasComp.removeAttribute('class');

	const borderGuntingComp = document.getElementById('border-gunting-comp');
	borderGuntingComp.removeAttribute('class');

	} else if( pilihanComputer == '.gunting-img-comp' ) {
		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.setAttribute('class', 'border-gunting-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.removeAttribute('class');

	} else if( pilihanComputer == '.kertas-img-comp' ) {
		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.setAttribute('class', 'border-kertas-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.removeAttribute('class');
    };
});

const playerGunting = document.querySelector('.gunting');
playerGunting.addEventListener('click', function() {
    playerGunting.style.backgroundColor = "#C4C4C4";
    playerGunting.style.borderRadius = "15px";
    playerGunting.style.padding = "8px";

    const pilihanComputer = getPilihanComputer ();
    const pilihanPlayer = playerGunting.className;
    const hasil = getHasil(pilihanComputer, pilihanPlayer);
    const vs = document.querySelector('.hasil');
    vs.innerHTML = hasil;

    if( pilihanComputer == '.batu-img-comp' ) {
	const borderBatuComp = document.getElementById('border-batu-comp');
	borderBatuComp.setAttribute('class', 'border-batu-comp');

	const borderKertasComp = document.getElementById('border-kertas-comp');
	borderKertasComp.removeAttribute('class');

	const borderGuntingComp = document.getElementById('border-gunting-comp');
	borderGuntingComp.removeAttribute('class');

	} else if( pilihanComputer == '.gunting-img-comp' ) {
		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.setAttribute('class', 'border-gunting-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.removeAttribute('class');

	} else if( pilihanComputer == '.kertas-img-comp' ) {
		const borderKertasComp = document.getElementById('border-kertas-comp');
		borderKertasComp.setAttribute('class', 'border-kertas-comp');

		const borderBatuComp = document.getElementById('border-batu-comp');
		borderBatuComp.removeAttribute('class');

		const borderGuntingComp = document.getElementById('border-gunting-comp');
		borderGuntingComp.removeAttribute('class');
    };
});

const refresh = document.querySelector('.refresh');
refresh.addEventListener('click', function() {
    location.reload();
});






